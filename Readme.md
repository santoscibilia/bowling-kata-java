# Principi del Kata

_Kata_ o "forma" indica, nelle arti marziali, la pratica consistente nell'esercitare le mosse dell'arte facendo la massima attenzione alla corretta esecuzione, al di fuori del contesto del combattimento. Lo scopo è raggiungere la massima padronanza dell'essenza della mossa stessa, appunto la sua "forma".

Nella programmazione, si chiama _Code Kata_ l'esercizio di risolvere un problema ponendo l'attenzione non sulla soluzione, ma sul percorso seguito per arrivarci e sull'esecuzione puntuale e rigorosa delle pratiche che si vogliono studiare.

# Specifiche dell'esercizio

Il problema su cui ci eserciteremo è il classico [Calcolo del punteggio del Bowling](http://codingdojo.org/cgi-bin/index.pl?KataBowling).

In breve, le regole sono le seguenti:

- il gioco del Bowling è diviso in Frames di uno o due tiri, tranne l'ultimo, il decimo, che può essere di tre tiri.
- se in un Frame non si abbattono tutti i birilli, il punteggio del frame è il totale dei birilli abbattuti
- se in un Frame si abbattono in due tiri tutti i birilli, il punteggio del frame è 10 più il punteggio del tiro successivo nel frame seguente.
- se in un Frame si abbattono tutti i birilli al primo tiro, il frame è concluso ed il suo punteggio è 10 più il punteggio dei due tiri successivi (nel o nei frame seguenti).
- nell'ultimo Frame, se si abbattono tutti e dieci i birilli al primo tiro o con il secondo, si ha diritto ad un terzo tiro.

Spesso, la trascrizione della sequenza dei tiri usa il carattere **-** per indicare zero birilli abbattuti, **/** per indicare che il secondo tiro ha abbattuto tutti i birilli rimanenti (_spare_), e **X** per indicare l'abbattimento di tutti i birilli con un tiro solo (_strike_).

# Specifiche tecniche

I test sono scritti usando dei **ParametrizedTests** di [JUnit](https://github.com/junit-team/junit4/wiki/Parameterized-tests). In questo modo le coppie (_valore di ingresso_, _risultato atteso_), modellate dalla classe **IntVal**, possono essere scritte come liste che vengono valutate da ciascun test. Formarvi e discutere la vostra opinione su questo modo di scrivere i test è parte integrante dell'esercizio.

Il progetto comprende un file **build.gradle** che lo definisce per il tool di build **Gradle** e dichiara le dipendenze necessarie e che permette di predisporre l'ambiente di lavoro su Eclipse. Altri ambienti di sviluppo non dovrebbero aver problemi a riconoscere e configurare quanto necessario. Da linea di comando, si possono eseguire i test con **gradle check**. 

I test, raccolti nella suite **AllTests**, si aspettano che la classe sottoposta al test si chiami **BowlingGame** e abbia due costruttori:

`public BowlingGame(java.util.List<Integer> vals)`
`public BowlingGame(java.lang.String vals)`

per ricevere il valore di input ed un metodo

`public int score()`

per ottenere il risultato.

I dati di ciascun test vengono passati al metodo opportuno ed il risultato confrontato con le aspettative. Ogni test è costituito da una lista di coppie (_valore di ingresso_, _risultato atteso_) e da un metodo che la passa alla classe sotto test per la verifica.

# Svolgimento dell'esercizio

**Prima dell'esercizio** è consigliato risolvere il problema autonomamente, per farsi un'idea generale e quindi avere, durante il Kata, più tempo a disposizione per riflettere sul metodo e sui passi seguiti nella costruzione della soluzione.

Per farlo è sufficiente clonare questo repository e posizionarsi direttamente al passo 6 con il comando

`git merge Step6`

A questo punto si può sviluppare la classe **BowlingGame** e lavorare per soddisfare tutti i test.

**Durante l'esercizio** il repository verrà aggiornato di passo in passo per mantenere tutti sullo stesso argomento. Quando verrà comunicato la presenza di un nuovo aggiornamento, lo si potrà ottenere con i comandi:

`git pull`
`git merge origin/step_<N>`

Essendo il contenuto di ciascun branch essenzialmente modifiche alla classe di test, il proprio codice non verrà toccato.

# Sequenza dei test

I test sono costruiti in questa sequenza, per indirizzare in una precisa direzione la costruzione della soluzione:

- passo 1: alcuni tiri come sequenza di interi
- passo 2: alcuni tiri, contenenti risultati "spare"
- passo 3: alcuni tiri, contenenti risultati "strike"
- passo 4: cambio di specifiche: i tiri sono rappresentati ora da una stringa
- passo 5: partita con ultimo frame normale, senza eventi speciali
- passo 6: partita completa, dove l'ultimo frame usa le regole particolari

