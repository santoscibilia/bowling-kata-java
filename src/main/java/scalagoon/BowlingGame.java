package scalagoon;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class BowlingGame {

	private List<Integer> _vals = null; // qui vengono salvati i punteggi nel
										// seguente
	// formato normalizzato (i valori vengono tutti convertiti in intero con le seguanti regole)
	// "-"=0
	// "/"=-1
    // il resto dei valori resta com'�
	@SuppressWarnings("unused")
	public BowlingGame(java.util.List<Integer> vals) {
		_vals = new ArrayList<Integer>();
		ListIterator<Integer> iterator = vals.listIterator();

		Integer i = 0;
		while (iterator.hasNext()) {
			Integer v = iterator.next();
			_vals.add(v);
			if (v < 10) {
				if (iterator.hasNext()) {
					Integer v2 = iterator.next();
					if (v2 + v == 10)
						_vals.add(-1);// � uno spare
					else
						_vals.add(v2);// non � uno spare
				}
			}

		}
	}

	@SuppressWarnings("unused")
	public BowlingGame(String vals) {
		_vals = new ArrayList<Integer>();
		for (int i = 0; i < vals.length(); i++) {
			char currentChar = vals.charAt(i);
			switch (currentChar) {

			case 'X': // non ho toUpperCase perch� ho visto che nei test non ci
						// sono 'x' minuscole
				_vals.add(10);
				break;
			case '-':
				_vals.add(0);
				break;
			case '/':
				_vals.add(-1);
				break;

			default: {
				Integer v = currentChar - 48; // sono rimasti solo numeri a una
												// cifra
				if ((v > 0) && (v < 10)) // per sicurezza controllo che il
											// carattere sia un numero a una
											// cifra
											// anche se non ci sarebbe bisogno
											// visto
											// che non ci sono stringhe
											// sbagliate
											// nei tests
					_vals.add(v);
				else
					_vals.add(0); // se per caso il carattere non � un numero da
									// 1 a 9 aggiungo 0
				break;
			}
			}

		}

	}

	public int score() {
		if (_vals == null)
			return 0;
		int sc = 0;
		int numFrame = 0; // mi serve perch� se sono al decimo frame e faccio
							// strike 'X' o spare '/' gli ultimi 2 tiri si
							// aggiungono senza seguire le regole
		ListIterator<Integer> iterator = _vals.listIterator();
		while (iterator.hasNext()) {
			Integer v = iterator.next();
			numFrame += 1;
			sc += v;
			 // per costruire il codice sotto ho utilizzato (per alcuni pezzi pi� complicati) dei diagrammi di stato
			// fatti carta e penna

				if (v < 10) {
					if (iterator.hasNext()) {
						Integer v2 = iterator.next();
						if (v2 == -1) {
							sc += (10 - v);
							// � uno spare e devo aggiungere il punteggio del
							// tiro
							// successivo
							if (iterator.hasNext()) {
								Integer v3 = iterator.next();
								sc += v3; // lo aggiungo tranquillamente perch�
											// non pu� essere un altro '/'
								if (numFrame<10) iterator.previous();// torno indietro per
													// continuare
													// il calcolo solo se non sono all'ultimo frame
							}

						}

						else {
							sc += v2;// non � uno spare aggiungo solo questo.
						}

					}
				} else { // siamo nel caso in cui ha fatto strike X
					if (iterator.hasNext()) {
						Integer v2 = iterator.next();// questo pu� essere solo
														// un 'X' o un numero ma
														// non '/' quindi lo
														// aggiungo
														// tranquillamente
						sc += v2;
						if (iterator.hasNext() && numFrame<10) {
							Integer v3 = iterator.next();
							if (v3 != -1) {
								sc += v3; // aggiungo entrambi i punteggi perch�
											// nei successivi 2 non ha fatto
											// spare
							} else {
								sc += (10 - v2); // aggiungo 10 perch� vuol dire
													// che nel tiro successivo
													// ha fatto spare (es. 3 / )
							}
							iterator.previous(); // torno indietro
						}
						if (numFrame<10){
							iterator.previous();// torno indietro per continuare il
							// calcolo							
						}
						
					}
				}
			
		}
		return sc;
	}

}
